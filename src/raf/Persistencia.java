package raf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class Persistencia {
	RandomAccessFile raf;
	String nomArchivo;
	File f;

	public Persistencia(String nomArchivo) throws FileNotFoundException {
		this.nomArchivo = nomArchivo;
		this.f = new File(nomArchivo);
		raf = new RandomAccessFile(f, "rw");
	}
	
	public Persistencia(File archivo) throws FileNotFoundException {
		this.nomArchivo = archivo.getName();
		this.f = archivo;
		raf = new RandomAccessFile(f, "rw");
	}

	public void cerrar() throws IOException {
		raf.close();
	}

	public void guardar(Cliente cli) throws IOException {
		// completar. Se debe guardar en el orden correspondiente.
		raf.writeShort(cli.getId());
		raf.writeChars(cli.getNombre().toString());
		raf.writeChars(cli.getApellidos().toString());
		raf.writeFloat(cli.getSaldo());
	}

	public Cliente leer() throws IOException {
		return leerRegistro();
	}

	private Cliente leerRegistro() throws IOException {
		short id = raf.readShort();
		String nombre = "";
		for (int i = 0; i < Cliente.TAM_NOMBRE; i++) {
			nombre = nombre + raf.readChar();
		}
		String apellidos = "";
		for (int i = 0; i < Cliente.TAM_APELLIDOS; i++) {
			apellidos = apellidos + raf.readChar();
		}
		float saldo = raf.readFloat();
		Cliente cli = new Cliente(id, nombre, apellidos, saldo);
		return cli;
	}

	public void irInicio() throws IOException {
		raf.seek(0);
	}

	public long totalRegistros() throws IOException {
		return raf.length() / Cliente.TAM_REGISTRO;
	}

	public void irFinal() throws IOException {
		raf.seek(raf.length());
	}

	public void irRegistro(int reg) throws IOException {
		raf.seek(reg * Cliente.TAM_REGISTRO - Cliente.TAM_REGISTRO);
	}

	public void borrarRegistro() throws IOException {
		Cliente cli = leerRegistro();
		if (cli.getId() > 0)
			cli.setId((short) (cli.getId() * (-1)));
		raf.seek(raf.getFilePointer() - Cliente.TAM_REGISTRO);
		guardar(cli);
	}

	// Ejercicio1: buscará por id de cliente
	public Cliente buscarPorID(short id) throws IOException {
		Cliente cliente;
		irInicio();
		for (int i = 1; i <= totalRegistros(); i++) {
			cliente = leerRegistro();
			if(cliente.getId()==id){
				return cliente;
			}
		}

		return null;
	}

	// Ejercicio2: buscará por aproximación en nombre y apellidos
	public List<Cliente> buscarPorNombre(String nombre) throws IOException {
		List<Cliente> clientes = new ArrayList<>();
		irInicio();
		for (int i = 1; i <= totalRegistros(); i++) {
			StringBuilder nombreCompleto = new StringBuilder();
			Cliente cliente = leerRegistro();
			nombreCompleto.append(cliente.getNombre());
			nombreCompleto.append(cliente.getApellidos());
			if(nombreCompleto.toString().contains(nombre)){
				clientes.add(cliente);
			}
		}

		return clientes;
	}

	// Ejercicio3
	public long compactar1() throws IOException {

		irInicio();
		String nomArchivoAux = "auxiliar.dat";
		File aux = new File(nomArchivoAux);
		RandomAccessFile rafAux = new RandomAccessFile(aux, "rw");

		long cantidadOriginal = totalRegistros();
		for (int i = 1; i <= totalRegistros(); i++) {
			Cliente cliente = leerRegistro();
			if(!cliente.estaBorrado()){
				rafAux.writeShort(cliente.getId());
				rafAux.writeChars(cliente.getNombre().toString());
				rafAux.writeChars(cliente.getApellidos().toString());
				rafAux.writeFloat(cliente.getSaldo());
			}
		}

		raf.close();
		f.delete();
		rafAux.close();
		f = new File(nomArchivo);
		aux.renameTo(f);
		aux.delete();
		raf = new RandomAccessFile(f, "rw");

		return cantidadOriginal-totalRegistros();

	}

	// Ejercicio4 (opcional)
	public long compactar2() throws IOException {
		int c = 0;
		long cantidadOriginal = totalRegistros();
		irInicio();
		for (int i = 1; i <= totalRegistros(); i++) {
			Cliente cliente = leerRegistro();
			if(!cliente.estaBorrado()){
				c++;
			}
		}
		raf.setLength((long) c *Cliente.TAM_REGISTRO);

		return cantidadOriginal-totalRegistros();
	}

}